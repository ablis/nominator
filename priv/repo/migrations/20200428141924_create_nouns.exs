defmodule Nominator.Repo.Migrations.CreateNouns do
  use Ecto.Migration

  def change do
    create table(:nouns) do
      add :word, :text, null: false
      add :tags, {:array, :text}
    end

    create unique_index(:nouns, [:word])
    create index(:nouns, [:tags], using: :gin)
  end
end
