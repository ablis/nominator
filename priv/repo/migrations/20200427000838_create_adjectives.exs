defmodule Nominator.Repo.Migrations.CreateAdjectives do
  use Ecto.Migration

  def change do
    create table(:adjectives) do
      add :word, :text, null: false
      add :tags, {:array, :text}
    end

    create unique_index(:adjectives, [:word])
    create index(:adjectives, [:tags], using: :gin)
  end
end
