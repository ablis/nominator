# Nominator

## Installation

### Database set-up

This application requires a [PostgreSQL][] database (version 10 or later). First
install PostgreSQL, then create a database user.

For local development purposes, a superuser by the name `nominator` can be
created by running `createuser -dPrs nominator` and then configuring the
password to match the one in `config.exs` (`nominator` by default).

For a production environment, be sure to pick a more secure password and
consider not using a superuser account.

## Usage

To start the Phoenix server:

* Setup the project with `mix setup`
* Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

* Official website: https://www.phoenixframework.org/
* Guides: https://hexdocs.pm/phoenix/overview.html
* Docs: https://hexdocs.pm/phoenix
* Forum: https://elixirforum.com/c/phoenix-forum
* Source: https://github.com/phoenixframework/phoenix
