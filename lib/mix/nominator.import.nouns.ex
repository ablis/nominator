defmodule Mix.Tasks.Nominator.Import.Nouns do
  alias Nominator.Repo
  alias Nominator.Import.Word
  alias Nominator.Words
  use Mix.Task

  @shortdoc "Import nouns from a CSV file"
  def run(args) do
    {:ok, _} = Application.ensure_all_started(:postgrex)
    {:ok, _} = Application.ensure_all_started(:ecto_sql)
    {:ok, _} = Repo.start_link()

    get_filepath_from_args(args)
    |> Word.words_from_csv_file()
    |> Stream.map(&Map.from_struct/1)
    |> Enum.each(&Words.create_noun/1)
  end

  def get_filepath_from_args([filepath]), do: filepath

  def get_filepath_from_args([]) do
    IO.puts("File name missing, please provide a file name like this:")
    IO.puts("% mix nominator.import.nouns path/to/words.csv")
    raise "No file name provided."
  end
end
