defmodule Nominator.Words do
  @moduledoc """
  The Words context.
  """

  import Ecto.Query, warn: false

  alias Nominator.Repo
  alias Nominator.Words.Adjective
  alias Nominator.Words.Noun

  @doc """
  Gets a single adjective.

  Raises `Ecto.NoResultsError` if the Adjective does not exist.

  ## Examples

      iex> get_adjective!(123)
      %Adjective{}

      iex> get_adjective!(456)
      ** (Ecto.NoResultsError)

  """
  def get_adjective!(id), do: Repo.get!(Adjective, id)

  @doc """
  Creates a adjective.

  ## Examples

      iex> create_adjective(%{field: value})
      {:ok, %Adjective{}}

      iex> create_adjective(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_adjective(attrs \\ %{}) do
    %Adjective{}
    |> Adjective.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a adjective.

  ## Examples

      iex> update_adjective(adjective, %{field: new_value})
      {:ok, %Adjective{}}

      iex> update_adjective(adjective, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_adjective(%Adjective{} = adjective, attrs) do
    adjective
    |> Adjective.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a adjective.

  ## Examples

      iex> delete_adjective(adjective)
      {:ok, %Adjective{}}

      iex> delete_adjective(adjective)
      {:error, %Ecto.Changeset{}}

  """
  def delete_adjective(%Adjective{} = adjective) do
    Repo.delete(adjective)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking adjective changes.

  ## Examples

      iex> change_adjective(adjective)
      %Ecto.Changeset{data: %Adjective{}}

  """
  def change_adjective(%Adjective{} = adjective, attrs \\ %{}) do
    Adjective.changeset(adjective, attrs)
  end

  @doc """
  Gets a single noun.

  Raises `Ecto.NoResultsError` if the Noun does not exist.

  ## Examples

      iex> get_noun!(123)
      %Noun{}

      iex> get_noun!(456)
      ** (Ecto.NoResultsError)

  """
  def get_noun!(id), do: Repo.get!(Noun, id)

  @doc """
  Creates a noun.

  ## Examples

      iex> create_noun(%{field: value})
      {:ok, %Noun{}}

      iex> create_noun(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_noun(attrs \\ %{}) do
    %Noun{}
    |> Noun.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a noun.

  ## Examples

      iex> update_noun(noun, %{field: new_value})
      {:ok, %Noun{}}

      iex> update_noun(noun, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_noun(%Noun{} = noun, attrs) do
    noun
    |> Noun.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a noun.

  ## Examples

      iex> delete_noun(noun)
      {:ok, %Noun{}}

      iex> delete_noun(noun)
      {:error, %Ecto.Changeset{}}

  """
  def delete_noun(%Noun{} = noun) do
    Repo.delete(noun)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking noun changes.

  ## Examples

      iex> change_noun(noun)
      %Ecto.Changeset{data: %Noun{}}

  """
  def change_noun(%Noun{} = noun, attrs \\ %{}) do
    Noun.changeset(noun, attrs)
  end
end
