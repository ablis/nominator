defmodule Nominator.Repo do
  use Ecto.Repo,
    otp_app: :nominator,
    adapter: Ecto.Adapters.Postgres
end
