defmodule Nominator.Words.Noun do
  use Ecto.Schema
  import Ecto.Changeset

  schema "nouns" do
    field :word, :string
    field :tags, {:array, :string}
  end

  @doc false
  def changeset(noun, attrs) do
    noun
    |> cast(attrs, [:word, :tags])
    |> validate_required([:word])
  end
end
