defmodule Nominator.Words.Adjective do
  use Ecto.Schema
  import Ecto.Changeset

  schema "adjectives" do
    field :word, :string
    field :tags, {:array, :string}
  end

  @doc false
  def changeset(adjective, attrs) do
    adjective
    |> cast(attrs, [:word, :tags])
    |> validate_required([:word])
  end
end
