defmodule Nominator.Import.Word do
  alias NimbleCSV.RFC4180, as: CSV

  @enforce_keys [:word]
  defstruct word: nil, tags: []

  def words_from_csv_file(path) do
    File.stream!(path, read_ahead: 1_000)
    |> CSV.parse_stream()
    |> Stream.map(&word_from_csv_line/1)
    |> Stream.map(&tag_word_initial/1)
  end

  def tag_word_initial(%Nominator.Import.Word{} = word) do
    initial = String.first(word.word) |> String.downcase()

    %Nominator.Import.Word{word | tags: ["initial_#{initial}" | word.tags]}
  end

  defp word_from_csv_line([word, tags]) do
    %Nominator.Import.Word{
      word: word,
      tags: String.split(tags) |> Enum.map(&String.trim/1)
    }
  end

  defp word_from_csv_line([word]) do
    %Nominator.Import.Word{word: word}
  end
end
